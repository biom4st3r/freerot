package com.biom4st3r.free_rotation;

import java.lang.reflect.Field;

import com.biom4st3r.biow0rks.BioLogger;

import net.minecraft.client.render.model.json.ModelVariant;
import net.minecraft.client.render.model.json.ModelVariantMap;
import net.minecraft.client.util.math.Rotation3;
import net.minecraft.util.Identifier;

/**
 * FreeModelVar
 */
public class ModelVarientFreeRot extends ModelVariant {
    public static BioLogger logger = new BioLogger("ModelVairentFreeRot");
    public int xRot;
    public int yRot;
    static int tWeight = 1;
    public ModelVarientFreeRot(Identifier location, Rotation3 rotation3, boolean uvLock, int weight, int xRot,
            int yRot) {
        super(location, rotation3, uvLock, tWeight++);
        this.xRot = xRot;
        this.yRot = yRot;
        // ModelVariantMap
    }

    @Override
    public Rotation3 getRotation() {
        return super.getRotation();
    }

    @Override
    public Identifier getLocation() {
        // TODO Auto-generated method stub
        return super.getLocation();
    }

    @Override
    public int getWeight() {

        return super.getWeight();
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return super.hashCode();
    }

    @Override
    public boolean isShaded() {
        // TODO Auto-generated method stub
        return super.isShaded();
    }
}