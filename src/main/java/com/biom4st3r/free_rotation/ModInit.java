package com.biom4st3r.free_rotation;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.minecraft.block.Block;
import net.minecraft.client.render.model.BakedModelManager;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModInit implements ModInitializer, ClientModInitializer
{

	@Override
	public void onInitializeClient() {
		
		
    }
    public static Block EXAMPLE_BLOCK;
    public static Item EXAMPLE_ITEM;

    public static boolean caught(String id,String contains)
    {
        if(id.contains(contains))
        {
            System.out.println(id);
            return true;
        }
        return false;
    }

	@Override
	public void onInitialize() {
        // LivingEntity
        EXAMPLE_BLOCK = Registry.register(Registry.BLOCK, new Identifier("freerot","example"), new ExampleBlock());
        EXAMPLE_ITEM = Registry.register(Registry.ITEM, new Identifier("freerot","example"), new BlockItem(EXAMPLE_BLOCK, new Item.Settings().group(ItemGroup.MISC)));
	}

}
