package com.biom4st3r.free_rotation.mixin;

import java.util.function.Function;

import com.biom4st3r.biow0rks.BioLogger;
import com.biom4st3r.free_rotation.BioBakeModel;
import com.biom4st3r.free_rotation.ModInit;
import com.biom4st3r.free_rotation.ModelVarientFreeRot;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.ModelBakeSettings;
import net.minecraft.client.render.model.ModelLoader;
import net.minecraft.client.render.model.json.JsonUnbakedModel;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.util.Identifier;

/**
 * JsonUnbakedModelBaker
 */
@Mixin(JsonUnbakedModel.class)
public abstract class JsonUnbakedModelMixin {
    private static BioLogger loggerr = new BioLogger("quadBaking");

    @Shadow
    public abstract SpriteIdentifier resolveSprite(String spriteName);

    @Inject(
        at = @At("RETURN"),
        method="bake(Lnet/minecraft/client/render/model/ModelLoader;Lnet/minecraft/client/render/model/json/JsonUnbakedModel;Ljava/util/function/Function;Lnet/minecraft/client/render/model/ModelBakeSettings;Lnet/minecraft/util/Identifier;Z)Lnet/minecraft/client/render/model/BakedModel;",
        cancellable = true)
    private void fuckYourBakery(ModelLoader loader, JsonUnbakedModel parent, Function<SpriteIdentifier, Sprite> textureGetter, ModelBakeSettings settings, Identifier id, boolean hasDepth,CallbackInfoReturnable<BakedModel> ci)
    {
        if(parent.id.contains("freerot"))
        {
            loggerr.debug(parent.id);
        }
        if(settings instanceof ModelVarientFreeRot)
        {
            loggerr.debug("BioModel Injected. xRot %s yRot %s", ((ModelVarientFreeRot)settings).xRot,((ModelVarientFreeRot)settings).yRot);
            ModelVarientFreeRot fr = (ModelVarientFreeRot) settings;
            // loggerr.debug("xRot %s yRot %s", fr.xRot,fr.yRot);
            ci.setReturnValue(new BioBakeModel(fr,ci.getReturnValue()));
            ci.cancel();
        }
        // ModInit.caught(parent.id, "freerot");
    }

}