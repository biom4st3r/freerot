package com.biom4st3r.free_rotation.mixin;

import java.lang.reflect.Type;

import com.biom4st3r.biow0rks.BioLogger;
import com.biom4st3r.free_rotation.ModelVarientFreeRot;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.client.render.model.ModelRotation;
import net.minecraft.client.render.model.json.ModelVariant;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;


/**
 * ModelElementDeserMxn
 */
@Mixin(ModelVariant.Deserializer.class)
public abstract class ModelVarientMixin {
    private static BioLogger loggerr = new BioLogger("ModeVarientFreeRot");
    int xRot = 0;
    int yRot = 0;

    @Shadow
    protected abstract ModelRotation deserializeRotation(JsonObject object);

    @Redirect(at = @At(
            value="INVOKE",
            target = "net/minecraft/client/render/model/json/ModelVariant$Deserializer.deserializeRotation(Lcom/google/gson/JsonObject;)Lnet/minecraft/client/render/model/ModelRotation;"),
        method="deserialize")
    protected ModelRotation freeDeserialRot(ModelVariant.Deserializer deserializer,JsonObject object) {
        this.xRot = 0;
        this.yRot = 0;
        int xRot = JsonHelper.getInt(object, "x",0);
        int yRot = JsonHelper.getInt(object, "y",0);
        if(object.has("freerot") && JsonHelper.getBoolean(object, "freerot"))
        {
            loggerr.debug("freeDeserialRot: xRot %s yRot %s", xRot,yRot);
            // ModInit.caught("yes", "yes");
            this.xRot = xRot;
            this.yRot = yRot;
            return ModelRotation.X0_Y0;
        }
        else
        {
            return deserializeRotation(object);
        }
    }

    @Inject(at = @At(value = "RETURN",shift = Shift.BEFORE),method = "deserialize",cancellable = true,locals = LocalCapture.CAPTURE_FAILHARD)
    public void freeRotDeserialize(JsonElement jsonElement, Type type, 
        JsonDeserializationContext jsonDeserializationContext,CallbackInfoReturnable<ModelVariant> ci,
        JsonObject object, Identifier identifier, ModelRotation modelRotation, boolean bl, int i)
    {
        System.out.println(identifier);
        if(object.has("freerot") && JsonHelper.getBoolean(object, "freerot"))
        {

            ModelVarientFreeRot varient = new ModelVarientFreeRot(identifier, ModelRotation.X0_Y0.getRotation(), bl, i, xRot, yRot);
            ci.setReturnValue(varient);
            ci.cancel();
        }

    } 


    
}