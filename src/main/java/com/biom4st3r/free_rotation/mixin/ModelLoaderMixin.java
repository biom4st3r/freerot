package com.biom4st3r.free_rotation.mixin;

import com.biom4st3r.free_rotation.ModInit;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.ModelBakeSettings;
import net.minecraft.client.render.model.ModelLoader;
import net.minecraft.util.Identifier;

/**
 * ModelLoaderMixin
 */
@Mixin(ModelLoader.class)
public abstract class ModelLoaderMixin {

    @Inject(at = @At("HEAD"),method="bake")
    public void illLoadMyOwnModelsThankYou(Identifier identifier, ModelBakeSettings settings, CallbackInfoReturnable<BakedModel> ci)
    {
        // ModInit.caught(identifier.toString(), "freerot");
    }

    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/client/render/model/json/ModelVariantMap.hasMultipartModel()Z"),
        method="loadModel",locals = LocalCapture.CAPTURE_FAILHARD)
    public void checkModelVariantMap(Identifier id, CallbackInfo ci)
    {
        // EnchantmentHelper
        // net/minecraft/enchantment/EnchantmentHelper.getEnchantments(Lnet/minecraft/item/ItemStack;)Ljava/util/Map;

        ModInit.caught(id.toString(), "freerot");
    }
    
}