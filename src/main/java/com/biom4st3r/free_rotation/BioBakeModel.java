package com.biom4st3r.free_rotation;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

import com.biom4st3r.biow0rks.BioLogger;
import com.biom4st3r.biow0rks.client.ClientHelper;

import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext.QuadTransform;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.ModelBakeSettings;
import net.minecraft.client.render.model.json.ModelItemPropertyOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Quaternion;
import net.minecraft.world.BlockRenderView;

/**
 * BioBakeModel
 */
public class BioBakeModel implements BakedModel, FabricBakedModel {
    public static BioLogger logger = new BioLogger("BioBakedModel");
    ModelVarientFreeRot settings;
    BakedModel model;
    public BioBakeModel(ModelBakeSettings settings, BakedModel model) {
        this.settings = (ModelVarientFreeRot) settings;
        this.model = model;
    }

    @Override
    public boolean isVanillaAdapter() {
        return false;
    }

    @Override
    public void emitBlockQuads(BlockRenderView blockView, BlockState state, BlockPos pos,
            Supplier<Random> randomSupplier, RenderContext context) 
        {

        Quaternion yQuat = new Quaternion(Vector3f.NEGATIVE_Y, settings.yRot, true);
        Quaternion xQuat = new Quaternion(Vector3f.NEGATIVE_X, settings.xRot, true);
        QuadTransform transform = (quadview)-> {
            Vector3f v3 = new Vector3f();
            for(int i = 0; i < 4; i++)
            {
                quadview.copyPos(i, v3);
                v3.rotate(yQuat);
                v3.rotate(xQuat);

                // v3.rotate(xQuat);
                // double xOff = (v3.getX() * Math.cos(Math.toRadians(-settings.yRot)))- (v3.getZ() * Math.sin(Math.toRadians(-settings.yRot)));
                // double zOff = (v3.getX() * Math.sin(Math.toRadians(-settings.yRot)))- (v3.getZ() * Math.cos(Math.toRadians(-settings.yRot)));

                quadview.pos(i, v3);
                

            }
            return true;
        };
        context.pushTransform(transform);
        context.fallbackConsumer().accept(model);
        context.popTransform();
    }

    @Override
    public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context) {

    }

    @Override
    public List<BakedQuad> getQuads(BlockState state, Direction face, Random random) {
        return null;
    }

    @Override
    public boolean useAmbientOcclusion() {
        return model.useAmbientOcclusion();
    }

    @Override
    public boolean hasDepth() {
        return model.hasDepth();
    }

    @Override
    public boolean isSideLit() {
        return model.isSideLit();
    }

    @Override
    public boolean isBuiltin() {
        return model.isBuiltin();
    }

    @Override
    public Sprite getSprite() {
        return model.getSprite();
    }

    @Override
    public ModelTransformation getTransformation() {
        return model.getTransformation();
    }

    @Override
    public ModelItemPropertyOverrideList getItemPropertyOverrides() {
        return model.getItemPropertyOverrides();
    }

    
}